#!/usr/bin/env python3
import keras
from keras import layers
"""
Simple toy network, 2 consecutive conv2D layers that can be fused
Using strides
"""


inp = keras.Input(shape=(19, 19, 3))
x = layers.Conv2D(4, (3, 3), strides=(2, 2), activation="linear", padding='valid')(inp)
out = layers.Conv2D(4, (3, 3), activation="linear", padding='valid')(x)
model = keras.Model(
    inputs=[inp],
    outputs=[out],
    name=__file__[:-3]
)
model.summary()
model.save(__file__.replace('.py', '.h5'))
