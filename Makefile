# Tool
TOOL=$(HOME)/.local/bin/convfuser
TOOL_DEPS=$(shell find convfuser -name '*.py')

# Models
MODEL_DIR=models
MODEL_CACHE_DIR=./model_cache/

# Output dir for plots etc
OUT_DIR=output
# output plot format, select from: [png, tex]
PLOT_FMT=png

# Test Input Data
DATA_DIR=./data
DATA_URLS=$(wildcard $(DATA_DIR)/*.url)
ALL_DATA=$(DATA_URLS:.url=)
DATA=$(firstword $(ALL_DATA)) #default input data
.PRECIOUS: $(ALL_DATA)

# Virtual Environment targets
VENV_DIR=.venv
VENV_ACT=$(VENV_DIR)/bin/activate
VENV=$(VENV_DIR)/.packages_installed
ACT=activate

# Docker Settings
DOCKER_IMAGE_SRV=registry.gitlab.com
DOCKER_USER:=ci_user
DOCKER_IMAGE=$(DOCKER_IMAGE_SRV)/lwaeijen/convfusion:latest

# Networks
BUILTIN_NETS= \
	ResNet50 \
	ResNet101 \
	ResNet152 \
	MobileNet \
	MobileNetV2 \
	InceptionV3 \
	Xception \
	VGG16 \
	VGG19

PY_NETS= $(wildcard $(MODEL_DIR)/*.py)
H5_NETS=$(PY_NETS:.py=.h5)

# Schedule selection
IDX=0


#####################################
# Test Rules

# Some small user tests
test: toy1 xception

#####################################
# Network Execution Rules

# Generic target to run an h5 model
define RUN_H5 =
$1: $(TOOL) $(TOOL_DEPS) $(DATA) $2 | $(OUT_DIR)
	$(TOOL) $(EXTRA_ARGS) \
	   --h5-model=$(strip $2) \
	   --input-image=$(DATA) \
	   --idx=$(IDX) \
	   --dot=$(OUT_DIR)/$(strip $1).dot \
	   --dot-seg=$(OUT_DIR)/$(strip $1)-segmentgraph.dot \
	   --save-plot=$(OUT_DIR)/$(strip $1).$(PLOT_FMT) \
	   --hide-results \
	   $(shell [ -e $(OUT_DIR)/$(strip $1).ds ] && echo '--space=$(OUT_DIR)/$(strip $1).ds' || echo '--save-space=$(OUT_DIR)/$(strip $1).ds')
	@sed -i -e 's/name=\([^"][a-z_0-9:A-Z]*\)/name="\1"/g' $(OUT_DIR)/$(strip $1).dot
	@sed -i -e 's/name=\([^"][a-z_0-9:A-Z]*\)/name="\1"/g' $(OUT_DIR)/$(strip $1)-segmentgraph.dot
endef

# Define targets for all h5 models
$(foreach H5,$(H5_NETS),$(eval $(call RUN_H5,\
	$(notdir $(H5:.h5=)),\
	$(H5)\
)))

# Generic target to run a keras builtin model
define RUN_BUILTIN =
$1: $(TOOL) $(TOOL_DEPS) $(DATA) | $(MODEL_CACHE_DIR) $(OUT_DIR)
	$(TOOL) $(EXTRA_ARGS) \
	   --network=$(strip $2) \
	   --input-image=$(DATA) \
	   --idx=$(IDX) \
	   --dot=$(OUT_DIR)/$(strip $1).dot \
	   --dot-seg=$(OUT_DIR)/$(strip $1)-segmentgraph.dot \
	   --save-plot=$(OUT_DIR)/$(strip $1).$(PLOT_FMT) \
	   $(shell [ -e $(OUT_DIR)/$(strip $1).ds ] && echo '--space=$(OUT_DIR)/$(strip $1).ds' || echo '--save-space=$(OUT_DIR)/$(strip $1).ds') \
	   --model-cache=$(MODEL_CACHE_DIR)
	@sed -i -e 's/name=\([^"][a-z_0-9:A-Z]*\)/name="\1"/g' $(OUT_DIR)/$(strip $1).dot
	@sed -i -e 's/name=\([^"][a-z_0-9:A-Z]*\)/name="\1"/g' $(OUT_DIR)/$(strip $1)-segmentgraph.dot
endef

# Create lower case shorthands for each network by name
$(foreach NET,$(BUILTIN_NETS),$(eval $(call RUN_BUILTIN,\
   $(shell echo '$(NET)' | tr 'A-Z' 'a-z'),\
   $(NET)\
)))

#####################################
# Python to h5
$(MODEL_DIR)/%.h5:$(MODEL_DIR)/%.py
	./$<

#####################################
# Directory Creation
$(MODEL_CACHE_DIR) $(OUT_DIR):
	mkdir -p $@

#####################################
# Tool Installation Rule
install:$(TOOL)
$(TOOL):
	pip3 install -U -e $(CURDIR)

uninstall:
	pip3 uninstall -y convfuser 2>&1 >/dev/null || true

#####################################
#  Collect input data
$(DATA_DIR)/%:$(DATA_DIR)/%.url
	wget $$(cat $<) -O $@ || rm $@


#####################################
# Virtual Environment Build Rules

venv: $(VENV)

$(VENV_ACT):
	virtualenv -p python3 $(VENV_DIR)

$(VENV):python_packages $(VENV_ACT)
	bash -c '$(ACT) && pip3 install -r $< && touch $@'


#####################################
# Docker Build Rules

docker-build:docker/Dockerfile python_packages
	cp python_packages docker/
	nice -n 10 docker build $(DOCKER_EXTRA) -t $(DOCKER_IMAGE) docker

docker-pull:
	docker login $(DOCKER_IMAGE_SRV) -u $(DOCKER_USER) -p $(shell pass gitlab.com/registry_token)
	docker pull $(DOCKER_IMAGE)

docker-start:
	@docker run \
	    --user convfusion:users \
	    --rm --tty --interactive \
	    --volume $(CURDIR):/convfusion:z --workdir /convfusion \
	    -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
		--entrypoint /bin/bash \
	    $(DOCKER_IMAGE)


#####################################
# Cleanup
.PHONY: clean test install uninstall venv docker-build docker-pull docker-start
clean: uninstall
	rm -f docker/python_packages
	rm -rf $(VENV_DIR)
	rm -rf $(OUT_DIR)
	rm -f $(ALL_DATA)
	rm -f $(addsuffix .dot,$(NETS))
	rm -f $(H5_NETS)
	#rm -rf $(MODEL_CACHE_DIR)
