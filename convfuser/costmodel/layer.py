from .costs import cons_vol, times, wacc, dbuf, wbuf, macs, obuf, oacc
warned = False


def layer(l, s, prod_vol=None):
    """ Cost of a layer"""
    global warned
    l = l.cfg
    if prod_vol is None:
        prod_vol = l.prod_vol

    # assert tile sizes do not exceed dimensions of prod_vol!!
    for lvl, d in zip('mno', prod_vol):
        if warned and s.T[lvl] <= d:
            print('\n'.join([
                "Warning: T%s (%d) of layer %s exceeds "
                "the required production volume in %s (%d)." % (
                    lvl, s.T[lvl], l.name, lvl, d
                ),
                "Model results may be inaccurate.",
                "N.B. this warning is only shown once."
            ]))
            warned = True

    return (
        cons_vol(l, s),         # elements consumed in a single transfer
        cons_vol(l, s, folded=True),         # elements consumed in a single transfer
        times(l, s, prod_vol),  # number of times this volume is transfered
        times(l, s, prod_vol, folded=True),  # number of times this volume is transfered
        wacc(l, s, prod_vol),   # weight accesses
        oacc(l, s),             # output accesses
        dbuf(l, s),             # data buffer size
        wbuf(l, s),             # weight buffer size
        obuf(l, s),             # output buffer size
        macs(l, s, prod_vol)    # multiply accumulates required to produce one production volume
    )
