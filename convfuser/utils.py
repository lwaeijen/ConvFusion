from math import ceil, floor, log
import numpy as np
from functools import reduce
from scipy import spatial
import re


def tex_escape(text):
    """
        :param text: a plain text message
        :return: the text escaped to appear correctly in LaTeX
    """
    # Source: https://stackoverflow.com/a/25875504
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(str(key)) for key in sorted(conv.keys(), key=lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)


def max2consec(l):
    """Return maximum sum of 2 consecutive elements"""
    if len(l) > 1:
        return max(map(sum, zip(l, l[1:])))
    if len(l) == 1:
        return l[0]
    return 0


def acc(*args):
    """accumulate reduction function"""
    return sum(args)


def reduce_init(f, l, init):
    """
    Reduce with explicit init valuei
    Useful when reduction l may be empty
    """
    def prefix(init, l):
        yield init
        for e in l:
            yield e
    return reduce(f, prefix(init, l))


def iceil(x):
    """Ceiling and return integer"""
    return int(ceil(x))


def ifloor(x):
    """Floor and return integer"""
    return int(floor(x))


def ceildiv(a, b):
    """division and ceiling"""
    return -(-a // b)


def floordiv(a, b):
    return a // b


def listify(l):
    """helper function: turn elements into list"""
    return l if isinstance(l, list) else [l]


def delistify(l):
    """return element iff list contains exacty one element
       else return list unmodified"""
    if len(l) == 1:
        return l[0]
    return l


def rmse(ref, cmp):
    return np.sqrt(np.mean((ref - cmp)**2))


class Namespace(dict):
    """Dictionary that can be addressed through attributes"""

    def __init__(self, *args, **kwargs):
        super(Namespace, self).__init__(*args, **kwargs)
        self.__dict__ = self


class InvisibleList(list):
    """Regular list with dummy comparisons so it can be passed
    to pareto and set functions, without contributing to the results
    """

    def __gt__(self, a):
        return False

    def __ge__(self, a):
        return True

    def __lt__(self, a):
        return False

    def __le__(self, a):
        return True

    def __eq__(self, a):
        return True

    def __float__(self):
        return float(0.0)

    def __hash__(self):
        return 0


# def pareto(pts, ignore_last_n_cols=1):
#    return pts

def pareto(costs, cols=-1):
    """
    :param costs: An (n_points, n_costs) array
    :maximise: boolean. True for maximising, False for minimising
    :return: A (n_points, ) boolean array, indicating whether each point is Pareto efficient
    """
    is_efficient = np.ones(costs.shape[0], dtype=bool)
    for i, c in enumerate(costs):
        if is_efficient[i]:
            is_efficient[is_efficient] = np.any(costs[is_efficient, :cols] < c[:cols], axis=1)  # Remove dominated points
            # N.B., since we require a point to be dominant in at least one dimension to stay, points would remove themselves
            # This can never happen:
            is_efficient[i] = True  # point itself should always stay
    return costs[is_efficient]


def pareto_hull(pts):
    """
    Iteratively filter points based on the convex hull heuristic
    """

    def filter_(pts, pt):
        """
        Get all points in pts that are not Pareto dominated by the point pt
        """
        strictly_worse = (pts >= pt).all(axis=-1)
        weakly_worse = (pts > pt).any(axis=-1)
        return pts[~(weakly_worse & strictly_worse)]

    def get_pareto_undominated_by(pts1, pts2=None):
        """
        Return all points in pts1 that are not Pareto dominated
        by any points in pts2
        """
        if pts2 is None:
            pts2 = pts1
        return reduce(filter_, pts2, pts1)

    pareto_groups = []

    # loop while there are points remaining
    while pts.shape[0]:
        # brute force if there are few points:
        if pts.shape[0] < 10:
            pareto_groups.append(get_pareto_undominated_by(pts))
            break

        # compute vertices of the convex hull
        hull_vertices = spatial.ConvexHull(pts).vertices

        # get corresponding points
        hull_pts = pts[hull_vertices]

        # get points in pts that are not convex hull vertices
        nonhull_mask = np.ones(pts.shape[0], dtype=bool)
        nonhull_mask[hull_vertices] = False
        pts = pts[nonhull_mask]

        # get points in the convex hull that are on the Pareto frontier
        pareto = get_pareto_undominated_by(hull_pts)
        pareto_groups.append(pareto)

        # filter remaining points to keep those not dominated by
        # Pareto points of the convex hull
        pts = get_pareto_undominated_by(pts, pareto)

    return np.vstack(pareto_groups)
