from ...utils import Namespace, InvisibleList
from .layer import Layer


class Segment(InvisibleList):
    """ A list of layer schedules which are executed in a fused manner"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.buffered_input = False
        self.recompute = False

    @property
    def nodes(self):
        return self[0]

    @property
    def names(self):
        return [n.name for n in self.nodes]

    @property
    def schedule(self):
        return self[1:]

    def node_sched(self):
        return zip(self.nodes, self.schedule)

    def __str__(self):
        return "[Segment: %s, Input: %sbuffered, Recompute: %s, Schedules<%d>]" % (
            str(self.names),
            '' if self.buffered_input else 'un',
            'true' if self.recompute else 'false',
            len(self.schedule)
        )

    def __repr__(self):
        return str(self)

    @property
    def json(self):
        """json compatible representation"""
        return {
            'nodes': dict(
                (name, sched.json) for name, sched in
                zip(self.names, self.schedule)
            ),
            'buffered': self.buffered_input,
            'recompute': self.recompute
        }

    @classmethod
    def from_json(cls, obj, name2node):
        # Start with fresh list
        ss = cls([[]])
        for name, sched_json in obj['nodes'].items():
            ss[0] += [name2node[name]]
            ss += [Layer.from_json(sched_json)]
        ss.buffered_input = obj['buffered']
        ss.recompute = obj['recompute']
        return ss
