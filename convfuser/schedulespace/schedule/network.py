from .partition import Partition


class Network(tuple):
    def schedule_str(self):
        """String of the schedule"""
        s = ['Schedule:']
        for seg in self[-1]:
            s += ['  Segment: [%s]' % (', '.join(seg.names))]
            s += ['  - input: %sbuffered' % ('' if seg.buffered_input else 'un')]
            for node_name, sched in zip(seg.names, seg.schedule):
                s += ['  - %s:' % (node_name)]
                s += ['\n'.join(
                    map(lambda ss: '   +' + ss, str(sched).split('\n'))
                )]
        return '\n'.join(s)

    def __str__(self, space='  '):
        """String of the Schedule including costs"""
        s = [
            space + '#Accesses: %d' % (self[0]),
            'Memory Size: %d' % (self[1]),
            'Multiply accumulates: %d' % (self[2]),
            self.schedule_str()
        ]
        return ('\n' + space).join(s)

    def __repr__(self):
        return str(self)

    @property
    def names(self):
        return ['accesses', 'memory', 'macs', 'schedule']

    @property
    def json(self):
        d = dict(zip(self.names[:-1], self))
        d['schedule'] = self[-1].json
        return d

    @classmethod
    def from_json(cls, obj, name2node):
        return cls((
            obj['accesses'],
            obj['memory'],
            obj['macs'],
            Partition.from_json(obj['schedule'], name2node)
        ))
