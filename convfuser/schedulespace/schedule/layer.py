from ...utils import Namespace


class Layer(object):
    """ Schedule of a single layer"""

    def __init__(self, sl, order, T):
        self._sl = sl
        self._order = order
        self._T = T

        # unconditional precompute
        self.dims = self.__dims
        self.dim_names = self.__dim_names
        self.order = self.__order

        if self._sl is not None and self._T is not None:
            self.precomp()

    def precomp(self):
        # precompute to speed up
        self._cl = self.__cl
        self.sl = self.__sl
        self.T = self.__T
        self.cl = self.___cl

        # create LUT for loopidx
        self.loopidx_lut = {}
        for lvl in self.dim_names:
            self.loopidx_lut[lvl] = self.__loopidx(lvl)
        for lvl in range(self.dims):
            self.loopidx_lut[lvl] = self.__loopidx(lvl)

    @property
    def __dims(self):
        return len(self._order) // 2

    @property
    def __dim_names(self):
        return 'mnoi'[0:self.dims]

    @property
    def __T(self):
        return Namespace(**dict(zip(self.dim_names, self._T)))

    @property
    def __order(self):
        return [
            self.__idx2name(pos) if isinstance(pos, int) else pos
            for pos in self._order
        ]

    @property
    def __sl(self):
        return Namespace(**dict(zip(
            ['w', 'd'],  # keys
            map(lambda i: self.order[i], self._sl)  # map level to name
        )))

    @property
    def __cl(self):
        # compute level weights
        slw, sld = self._sl
        order = self.order

        cl_w = slw
        for offset in range(1, 3):
            if slw >= offset and order[slw - offset][0] in 'oi':
                cl_w = slw - offset
                continue
            break

        # compute level data
        cl_d = sld
        for offset in range(1, 3):
            if (
                sld >= offset and
                order[sld - offset][0] in
                ('imn' if offset == 1 else 'mn')
            ):
                cl_d = sld - offset
                if order[sld - offset][0] in 'mn':
                    break
                continue
            break

        return (cl_w, cl_d)

    @property
    def ___cl(self):
        """Compute levels"""
        return Namespace(**dict(zip(
            ['w', 'd'],  # keys
            map(lambda i: self.order[i], self._cl)  # map level to name
        )))

    def name2idx(self, name):
        return self.dim_names.index(name)

    def __idx2name(self, lvl):
        return self.dim_names[lvl % self.dims] + \
            ('_i' if lvl < self.dims else '_o')

    def __loopidx(self, lvl):
        idx = lvl
        if isinstance(lvl, str):
            idx = self.name2idx(lvl)
        return self._order.index(idx)

    def loopidx(self, lvl):
        return self.loopidx_lut[lvl]

    def lt(self, lvl1, lvl2):
        return self.loopidx_lut[lvl1] < self.loopidx_lut[lvl2]

    def lt_sl(self, lvl, data=True):
        return self.loopidx_lut[lvl] < self._sl[int(data)]

    def lt_cl(self, lvl, data=True):
        return self.loopidx_lut[lvl] < self._cl[int(data)]

    def fold_dims(self, data=True):
        """Return the dimensions that can be folded"""
        cl = self._cl[int(data)]
        sl = self._sl[int(data)]
        return [lvl[0] for lvl in self.order[cl:sl]]

    def __hash__(self):
        hash(str(self))

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        dims = self.dims
        dim_names = self.dim_names

        s = []
        s += ['Order:       ' + ', '.join(self.order)]

        s += ['Store Lvls:  ' + '%s (weights) - %s (data)' % tuple(map(
            lambda sl: self.__idx2name(self._order[sl]), self._sl))
        ]
        s += ['Tiling:      ' + ', '.join(
            ['T%s=%d' % (n, d) for (n, d) in zip(dim_names, self._T)]
        )]

        return '\n'.join(s)

    @property
    def json(self):
        return {
            'store_level': self.sl,
            'order': self.order,
            'tiling': self.T
        }

    @classmethod
    def from_json(cls, obj):
        def names2tuple(ls, names):
            return tuple([
                ls.name2idx(n[:1]) if len(n) == 1 or n[-1] == 'i' else ls.dims
                for n in names
            ])

        # init order with list of correct length
        ls = cls(None, obj['order'], None)

        # Now load properly
        ls._order = tuple([
            ls.name2idx(l[:1]) + (ls.dims if l[-1] == 'o' else 0)
            for l in obj['order']
        ])
        # Load store levels
        ls._sl = (
            ls.order.index(obj['store_level']['w']),
            ls.order.index(obj['store_level']['d'])
        )
        # load tilings
        ls._T = tuple([obj['tiling'][k] for k in ls.dim_names])

        # manually trigger update of all variables
        ls.precomp()

        return ls
