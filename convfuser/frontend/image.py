import numpy as np
from PIL import Image


def load_image(fname, shape=(224, 224, 3), fmt='NCHW'):

    # Decode format
    if fmt == 'NCHW':
        size = shape[1:]
        channels = shape[0]
        channel_idx = 0
    elif fmt == 'NWHC':
        size = shape[:2]
        channels = shape[-1]
        channel_idx = -1

    # Load input image (RGB?)
    img = Image.open(fname).resize(reversed(size))

    # Image to numpy array
    x = np.array(img)[np.newaxis, :, :, :]

    # From channel last to channel first
    if fmt == 'NCHW':
        x = np.moveaxis(x, 3, 2)

    # Check number of channels
    assert(
        x.shape[channel_idx] == channels and
        "Error: image channels do not match network"
    )

    return x

