import keras
import networkx as nx
from os import path
import types
from .layerconfig import LayerCfgConv2D, LayerCfgBase
from ..utils import listify
from .canonicalizer import canonicalize as canonicalizer


def dot(self, fname):
    return nx.drawing.nx_pydot.write_dot(self, fname)


def model2graph(model, canonicalize=True):
    """Convert Keras Model to Internal Graph Representation"""

    # Convert keras model to graph
    g = nx.DiGraph()
    tensor_refs_to_producer = {}
    for l in model.layers:

        # Override class __str__ method, particularly useful for plotting
        l.__class__.__str__ = lambda self: \
            "%s (%s)" % (self.name, self.__class__.__name__)

        # set type of the layer based on it's class name
        l.type = l.__class__.__name__.lower()

        # Add configuration to layer if Conv2D
        CfgClass = LayerCfgConv2D if l.type in ['conv2d'] else LayerCfgBase
        setattr(l, 'cfg', CfgClass(l))

        # Add a pointer to a merged input node
        setattr(l, 'merged_in', None)

        # And add a pointer to a merged input node
        setattr(l, 'merged_out', None)

        # Add layer to graph
        g.add_node(l)

        # Populate map of output tensors to layer
        for output in listify(l.output):
            tensor_refs_to_producer[output.ref()] = l

        # connect new layer to all inputs
        for tensor in listify(l.input):
            if tensor.ref() in tensor_refs_to_producer:
                src = tensor_refs_to_producer[tensor.ref()]
                dst = l

                # no self loops
                if (src == dst):
                    continue

                # add edge to network
                g.add_edge(src, dst, name=str(tensor.name))
                # print('conn', src.name, '->', dst.name)

    # clean up
    del tensor_refs_to_producer

    # Canonicalize graph
    if canonicalize:
        canonicalizer(g)
    else:
        print(
            'WARNING: canonicalization turned off. '
            'Possibly less opportunities for layer fusion'
        )

    # add plotting functionality to graph
    g.dot = types.MethodType(dot, g)

    # find last nodes in the network
    leafs = [n for n in g.nodes if g.out_degree(n) == 0]
    assert(
        len(leafs) == 1 and
        "Only support for networks with a single exit point"
    )
    g.last = leafs[0]

    # find input nodes of the network
    inputs = [n for n in g.nodes if n.__class__.__name__ in ['InputLayer']]
    assert(
        len(inputs) == 1 and
        not isinstance(inputs[0].input, list) and
        "Only support for networks with a single entry point"
    )
    g.first = inputs[0]
    g.input_shape = g.first.input.shape[1:]

    # Return graph object
    return g


def load_h5(fname, **kwargs):
    """Load keras model from h5 file"""

    # Load keras model from file
    model = keras.models.load_model(fname)

    return model2graph(model, **kwargs), model


def load_builtin(name, cache_dir=None, **kwargs):
    """Load builtin network from keras"""
    import keras.applications as apps
    import re

    # Workaround some quircks in naming
    helper_name = name.lower()

    # for any resnet, pre/post processing lives under resnet50
    if helper_name.startswith('resnet'):
        helper_name = 'resnet50'

    # V3 and V2 networks etc need an extra underscore
    helper_name = re.sub(r'v([0-9]+)', '_v\g<1>', helper_name)

    # Dynamically Load post/pre processing
    preprocess = getattr(getattr(apps, helper_name), 'preprocess_input')
    postprocess_orig = getattr(getattr(apps, helper_name), 'decode_predictions')

    # Wrap default post processing into one that makes a pretty string
    def postprocess(*args, **kwargs):
        out = []
        for b_idx, batch in enumerate(postprocess_orig(*args, **kwargs)):
            out += ['Batch %d:' % (b_idx)]
            width = max(map(lambda b: len(b[1]), batch))
            for node, name, prob in batch:
                out += ["  - %s  (%5.2f %%)" % (
                    name.ljust(width, ' '),
                    prob * 100.
                )]
        return '\n'.join(out)

    # Compute name for caching model
    fname = "%s.h5" % (
        path.join(
            cache_dir if cache_dir else '',
            name.lower()
        )
    )

    # Load model
    if(path.exists(fname) and cache_dir):
        # From h5 if cached
        print('Loading from model cache...')
        model = keras.models.load_model(fname)
    else:
        # Dynamically load imagenet model if not cached
        Net = getattr(apps, name)
        model = Net(weights='imagenet')

    # Cache if required
    if cache_dir and not path.exists(fname):
        model.save(fname)

    return model2graph(model, **kwargs), model, preprocess, postprocess
