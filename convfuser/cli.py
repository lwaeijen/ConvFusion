#!/usr/bin/env python3
import argparse
from .frontend import load_h5, load_builtin, load_image
from .schedulespace import scheduling_space, DesignSpace
from .schedulespace.layer import pow2tiles, wholetiles, notiling, extremestiles
from .costmodel import network as network_cost
from .backend import run
from sys import exit


def parse_args():
    parser = argparse.ArgumentParser()

    inp = parser.add_argument_group(title='Input Settings')

    network_group = inp.add_mutually_exclusive_group(required=True)

    # Use builtin network
    networks = [
        'ResNet50',
        'ResNet101',
        'ResNet152',
        'MobileNet',
        'MobileNetV2',
        'VGG16',
        'VGG19',
        'InceptionV3',  # graph exec error, or due to resizing?
        'Xception'  # rmse error
    ]
    network_group.add_argument(
        "--network",
        dest="network",
        choices=networks,
        default=None,
        help="Use builtin network on imagenet"
    )

    # Use external h5 model
    network_group.add_argument(
        "--h5-model",
        dest='h5_model',
        default=None,
        help="Keras h5 model file"
    )

    # model cache dir
    inp.add_argument(
        "--no-canonicalize",
        dest='canonicalize',
        action='store_false',
        help="Disable canonicalization of network graph"
    )

    # model cache dir
    inp.add_argument(
        "--model-cache",
        dest='model_cache_dir',
        required=False,
        default=None,
        help="Optional directory for caching builtin models"
    )

    # Input image
    inp.add_argument(
        "--input-image",
        dest='input_image',
        required=True,
        help="Input image"
    )

    # Use cached exploration file
    inp.add_argument(
        "--space",
        dest='space_fname',
        required=False,
        default=None,
        help="Optional database file with\
        previously explored scheduling space"
    )

    ds = parser.add_argument_group(
        title='Schedule Space Search Settings'
    )

    def int_ge1(x):
        x = int(x)
        if x >= 1:
            return x
        raise argparse.ArgumentTypeError("%d is not >=1" % (x))

    ds.add_argument(
        "--max-fused",
        dest='max_fused',
        required=False,
        default=None,
        type=int_ge1,
        help="Maximum number of fused layers (>=1)"
    )

    ds.add_argument(
        "--min-fused",
        dest='min_fused',
        required=False,
        default=None,
        type=int_ge1,
        help="Minimum number of fused layers (>=1)"
    )

    ds.add_argument(
        "--exploration-cap",
        dest='exp_cap',
        required=False,
        default=None,
        type=int,
        help="Abort segment exploration after examining N schedules"
    )

    tiling_methods = {
        'none': notiling,
        'powers-of-two': pow2tiles,  # tiles are power of two
        'exact':
        wholetiles,  # tiles are exact multiples of the dimension
        'extremes': extremestiles,  # tiles of 1 or full dimension
    }
    ds.add_argument(
        "--tiling",
        dest="tiling_func",
        choices=tiling_methods.keys(),
        default='exact',
        required=False,
        help="Which tilings to consider (Default powers-of-two)"
    )

    ds.add_argument(
        "--no-loopreorder",
        dest='loop_reorder',
        action='store_false',
        help="Fix loop order to: [m,n,i,o]"
    )

    ds.add_argument(
        "--no-output-contrib",
        dest='no_output_buf',
        action='store_true',
        help="Ignore contribution of output (accesses & memory footprint)"
    )

    ds.add_argument(
        "--buffered",
        dest='buffered',
        action='store_true',
        help="Consider buffering complete first layer of each fused, \
            recomputed segment"
    )

    ds.add_argument(
        "--recompute",
        dest='recompute',
        action='store_true',
        help="Consider recomputation of fused layers"
    )

    ds.add_argument(
        "--no-pareto",
        dest='pareto',
        action='store_false',
        help="Return all points, not only pareto dominant."
    )

    out = parser.add_argument_group(title='Output Settings')

    # dot file of the network
    out.add_argument(
        "--dot",
        dest='dot_fname',
        required=False,
        default=None,
        help="Write out dotfile of network graph"
    )

    out.add_argument(
        "--dot-seg",
        dest='seg_dot_fname',
        required=False,
        default=None,
        help="Write out dotfile of graph with fused layers for selected point"
    )

    out.add_argument(
        "--save-space",
        dest='save_space_fname',
        required=False,
        default=None,
        help="Store database file with\
        explored scheduling space"
    )

    out.add_argument(
        "--save-plot",
        dest='plot_fname',
        required=False,
        default=None,
        help="Store pareto plot of design space"
    )

    out.add_argument(
        "--hide-results",
        dest='show_results',
        required=False,
        action='store_false',
        help="Hide post-processed network output"
    )

    exe = parser.add_argument_group(title='Execution Settings')

    # Point to use
    exe.add_argument(
        "--idx",
        dest='idx',
        required=False,
        default=None,
        type=int,
        help="Index of point to run.\
        When omitted or negative visual selection will be presented"
    )

    # Trace halide code
    exe.add_argument(
        "--trace",
        dest='trace',
        action='store_true',
        help="Halide trace accesses and memory sizes"
    )

    # Verify
    exe.add_argument(
        "--verify",
        dest='verify',
        action='store_true',
        help="Verifies ALL found pareto schedules"
    )

    exe.add_argument(
        "--debug",
        dest='debug',
        action='store_true',
        help="Print debug output on verification failure"
    )

    exe.add_argument(
        "--quiet", dest='quiet', action='store_true', help="Print less"
    )

    # parse arguments
    args = parser.parse_args()

    # resolve tiling method
    args.tiling_func = tiling_methods[args.tiling_func]

    # sanity checks
    if args.buffered and (not args.recompute):
        print("WARN: --buffered has no effect without --recompute")
    if (not args.loop_reorder) and (not args.recompute):
        print(
            "WARN: --no-loopreorder has no effect without --recompute"
        )

    return args


def load_nw(args):
    # Load network
    if args.h5_model:
        nw, keras_model = load_h5(
            args.h5_model, canonicalize=args.canonicalize
        )
        preprocess = (lambda x: x.astype('float32'))
        postprocess = (lambda x: x)
    elif args.network:
        nw, keras_model, preprocess, postprocess = \
            load_builtin(
                args.network,
                cache_dir=args.model_cache_dir,
                canonicalize=args.canonicalize
            )

    return nw, keras_model, preprocess, postprocess


def get_space(args, nw):
    # Get Scheduling Space From File, or Explore
    if args.space_fname:
        print('Loading scheduling space from', args.space_fname)
        points = DesignSpace.load(nw, args.space_fname)
        print('Loaded %d points' % len(points))
        return points

    # Find pareto schedules and costs
    points = scheduling_space(nw, args=args)
    npoints = len(points)
    print(('Found %d schedule' % (npoints)) +
          's' if npoints != 1 else '')

    return points


def get_indexes(args, points):
    # return index(es) of point(s) to execute
    if args.verify:
        return range(len(points))

    if args.idx is not None and args.idx >= 0:
        if args.idx >= len(points):
            print(
                'Schedule index out of bounds'
                'only %d schedules to select from' % (len(points))
            )
            exit(-1)
        return [args.idx]

    idx = points.select_point()
    if idx is None:
        print('No point selected for execution, exiting...')
        exit(0)
    return [idx]


def verify(predicted, measured, quiet=False):
    # check predictions with measurements
    acc, mem, macs = predicted
    macc, mmem, mmacs = measured

    if (macc != acc):
        print('Modelled and measured data accesses do NOT match:')
        print('Modelled', acc)
        print('Measured:', macc)
        return False
    if not quiet:
        print('Modelled and measured accesses match!')

    if (mmem != mem):
        print('Modelled and measured buffer sizes do NOT match:')
        print('Modelled', mem)
        print('Measured:', mmem)
        return False
    if not quiet:
        print('Modelled and measured buffer sizes match!')

    if (mmacs != macs):
        print(
            'Modelled and measured multiply-accumulates do NOT match:'
        )
        print('Modelled', macs)
        print('Measured:', mmacs)
        return False
    if not quiet:
        print('Modelled and measured buffer sizes match!')

    if quiet:
        print('Modelled and measured parameters match!')
    return True


def main():

    # Parse CLI arguments
    args = parse_args()

    # Load network
    nw, keras_model, preprocess, postprocess = load_nw(args)

    # write out dotfile if requested
    if args.dot_fname:
        nw.dot(args.dot_fname)

    # Load and preprocess input image
    img = preprocess(
        load_image(args.input_image, fmt='NWHC', shape=nw.input_shape)
    )

    # Get scheduling space
    points = get_space(args, nw)

    # Potentially save space
    if args.save_space_fname:
        print('Saving scheduling space to', args.save_space_fname)
        points.save(args.save_space_fname)

    # Save pareto front if required
    if args.plot_fname is not None:
        points.plot(args.plot_fname, show=False)

    # Select points and execute
    for idx in get_indexes(args, points):
        # extract point
        point = points[idx]
        acc, mem, macs, sched = point

        # print point as feedback to user
        if not args.quiet:
            print(
                'Selected schedule [%d/%d]:\n%s' %
                (idx + 1, len(points), str(point))
            )
        else:
            print('Selected schedule [%d/%d]' % (idx + 1, len(points)))

        # Debug: recalculate network cost
        if args.debug:
            acc2, mem2, macs2 = network_cost(sched, verbose=False)
            print(acc2, mem2, macs2)
            if acc2 != acc or mem2 != mem or macs2 != macs:
                print("ERROR: stored costs don't match analysis")
                network_cost(sched, verbose=True)
                exit(-1)

        # Execute network
        res, stats = run(
            nw,
            sched,
            img,
            trace=args.trace or args.verify,
            seg_dot=args.seg_dot_fname,
            silent=args.quiet
        )

        # Post process the results
        if args.show_results:
            print(postprocess(res))

        # If there was no tracing, it ends here
        if not args.trace and not args.verify:
            exit(0)

        # Verify model against measurments, but first recompute without output
        # contributions. Halide does not allow measuring of output contrib.
        acc, mem, macs = network_cost(
            sched, ignore_output=True, verbose=False
        )
        correct = verify((acc, mem, macs), stats, quiet=args.quiet)
        if not correct:
            if args.debug:
                print('Debug Run:')
                res, stats = run(
                    nw,
                    sched,
                    img,
                    trace=True,
                    verbose=True,
                    silent=False,
                )
                print('Debug Modelled:')
                network_cost(
                    sched,
                    ignore_output=True,
                    verbose=True,
                    silent=False
                )
            exit(-1)

    # life is good
    exit(0)


if __name__ == '__main__':
    # need main as a function so it can be used as entry-point
    # when this file is called directly, simply call main
    main()
